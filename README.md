# Flutter Mason

Projek ini dibuat sebagai contoh implementasi package mason

## Get Started

1. Install `mason`

   ```sh
   dart pub global activate mason_cli
   ```

2. Check `mason` dan brick yang sudah tersedia
   ```sh
   mason --version
   mason get
   mason list
   ```

## How to create template / brick

Dengan menggunakan command

```sh
mason new [argument]
```

## How to generate template / brick

Dengan menggunakan command

```sh
mason make [argument] -o [target generate] or "../lib"
```

## Keunggulan

- mason bisa digunakan sebagai generator template
- berguna jiga mempunyai template page yang digunakan secara umum misalkan saya ingin membuat apps ecommerce dimana sudah pasti ada page seperti :

  1. login
  2. register
  3. cart
  4. akun

Dan masih banyak page lainnya, yang nantinya kita hanya perlu adjust sesuai UI yang dibutuhkan jika ada perbedaan
